<?php
class Arguments extends Tobjects
{
	protected static $search_keys = array('backtrace_arguments.first_name', 'backtrace_arguments.last_name');
	protected static $table_name = 'backtrace_arguments';

	public static function loadAll($order_by='', $limit='')
	{
		return parent::loadByParameters('', $order_by, $limit);
	}

	/***
	 * This function loads an object based on the parameters passed
	 * It requires parameters be defined in terms of column name, condition and value
	***/
	public static function loadByParameters($parameters, $order_by='', $limit='')
	{
		return parent::loadByParameters($parameters, $order_by, $limit);
	}
		
	/***
	 * This function performs search on the specified columns and loads Arguments that match teh search result
	***/	
	public static function searchLoad($search, $order_by='', $limit='')
	{		
		$where = array();
		$where[] = self::getSearchLoadWhere($search);
		return self::load(implode(' AND ', $where), true, $order_by, $limit);
	}
		
	public static function searchLoadByParameters($search, $parameters, $order_by='', $limit='')
	{
		return parent::searchLoadByParameters($search, $parameters, $order_by, $limit);
	}
		
	protected static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='')
	{
		if($where!='')
			$where='WHERE '.$where;
		
		if($order_by!='')
			$order_by='ORDER BY '.$order_by;
		
		$sql_calc_found_rows='';
		if($limit!='')
		{
			$limit='LIMIT '.$limit;
			$sql_calc_found_rows='SQL_CALC_FOUND_ROWS';
		}
		
		$sql=
			'SELECT DISTINCT '.$sql_calc_found_rows.'
				backtrace_arguments.id,
				backtrace_arguments.backtraces_id,
                argument_number,
                argument
			FROM
				arguments
			'.$where.'
			'.$order_by.'
			'.$limit;
		
		self::$found_rows=0;
		$data=array();
		if($rs = pdologged_preparedQuery($sql, $values))
		{
			if(!empty($limit))
			{
				$rs_count = pdologged_query('SELECT FOUND_ROWS()');
				if($row = $rs_count->fetch(PDO::FETCH_NUM))
					self::$found_rows=$row[0];
			}
			else
				self::$found_rows = $rs->rowCount();
			
			while($row = $rs->fetch(PDO::FETCH_ASSOC))
			{			

				$argument = new Argument(array
				(
                    'id'=>$row['id'],
                    'argument_number'=>$row['argument_number'],
                    'argument'=>$row['argument'],

                    'backtrace'=>array
                    (
                        'id'=>$row['backtraces_id']
                    )
				));
				
				
				$data[] = $argument;
				
				if(!$return_array)
					return $data[0];
			}			
			
			return $data;
		}
		return false;
	}
}

class Argument extends Tobject
{
	protected $id, $argument_number, $argument, $backtrace;
	
	function __construct($properties=NULL)
	{		
		parent::__construct('arguments',$properties);

        if(isset($properties['backtrace']))
        {
            $this->backtrace = new Backtrace($properties['backtrace']);
        }
	}
	
	public function getId()
	{
		return intval($this->id);
	}

    /**
     * @return mixed
     */
    public function getArgument()
    {
        return $this->argument;
    }

    /**
     * @return mixed
     */
    public function getArgumentNumber()
    {
        return $this->argument_number;
    }

    /**
     * @return \Backtrace
     */
    public function getBacktrace()
    {
        return $this->backtrace;
    }



	public function addRecord()
	{
		$sql =
			'INSERT INTO backtrace_arguments
			(
			    backtraces_id,
				argument_number,
				argument
			)
			VALUES
			(
				:backtraces_id,
				:argument_number,
				:argument
			)';
			
		$values = array
        (
            ':backtraces_id'=>$this->backtrace->getId(),
            ':argument_number'=>$this->argument_number,
            ':argument'=>$this->argument
		);
		if(pdologged_preparedQuery($sql, $values))
		{
			$this->id=Tabmin::$db->lastInsertId();
			return true;
		}
		
		return false;
	}
	
	public function updateRecord()
	{
		$sql_simpleFields = $this->generateSimpleUpdateFields(array('backtraces_id'));
		
		$sql =
			'UPDATE arguments SET
				'.$sql_simpleFields->getStub().'
			WHERE id=:id';
		
		$values = $sql_simpleFields->getValues();
		$values[':id'] = $this->getId();
		//AlertSet::addArgument(var_export($sql, true));
		if(pdologged_preparedQuery($sql, $values) !== false)
			return true;
		return false;
	}
}
?>