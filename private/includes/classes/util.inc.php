<?
global $db;
if(!function_exists('sanitize'))
{
	function sanitize($dirtyTrash)
	{
		if(get_magic_quotes_gpc() == 1)
			return $dirtyTrash;
		return addslashes($dirtyTrash);
	}
}

if(!function_exists('dirtify'))
{
	function dirtify($cleanTrash)
	{
		if(get_magic_quotes_gpc() == 1)
		{
			if(!is_array($cleanTrash))
				return stripslashes($cleanTrash);
			return stripslashes_deep($cleanTrash);
		}
		return $cleanTrash;
	}
}

if(!function_exists('stripslashes_deep'))
{
	function stripslashes_deep($cleanTrash)
	{
		$cleanTrash = is_array($cleanTrash) ?
			array_map('stripslashes_deep', $cleanTrash) :
			stripslashes($cleanTrash);
		
		return $cleanTrash;
	}
}

if(!function_exists('htmlentities_deep'))
{
	function htmlentities_deep($dirtyTrash)
	{
		$dirtyTrash = is_array($dirtyTrash) ?
			array_map('htmlentities_deep', $dirtyTrash) :
			(is_string($dirtyTrash) ?
				htmlentities($dirtyTrash) :
				$dirtyTrash);
		
		return 	$dirtyTrash;
	}
}

if(!function_exists('randString'))
{
	function randString($length)
	{
		$string='';
		for ($i=0; $i<$length; $i++)
		{
			switch(mt_rand(1,3))
			{
				case 1:
					$string .= chr(mt_rand(48, 57));
				break;
				case 2:
					$string .= chr(mt_rand(65, 90));
				break;
				case 3:
					$string .= chr(mt_rand(97, 122));
				break;
			}
		}
		return ($string);
	}
}

if(!function_exists('isEmail'))
{
	function isEmail($email)
	{
		if(!preg_match('/^[\w-\.]+@([\w-]+\.)+[\w-]+$/',$email)>0)
			return false;
		return true;
	}
}

if(!function_exists('isPhone'))
{	
	function isPhone($phone)
	{
		if(!preg_match('/^((\(\d{3}\)?)|(\d{3}))([\s-.\/]?)(\d{3})([\s-.\/]?)(\d{4})$/',$phone)>0)
			return false;
		return true;
	}
}

if(!function_exists('linkURLs'))
{
	function linkURLs($text)
	{
		return preg_replace('/https?:\/\/[^\)<\s\n]*/', '<a href="$0" target="_blank">$0</a>', $text);
	}
}

if(!function_exists('formatMoney'))
{
	function formatMoney($cents)
	{
		return number_format(((int)($cents/100) .'.'. (abs($cents%100)<10 ? '0' : '') . abs($cents%100)),2);
	}
}

if(!function_exists('htmlentitiesUTF8'))
{
	function htmlentitiesUTF8($var)
	{
		return htmlentities($var, ENT_QUOTES, 'UTF-8') ;
	}
}

if(!function_exists('truncate_by_words'))
{
	function truncate_by_words($dirty_trash, $n, $add_ellipses = true)
	{
		if(preg_match('/(\S+\s*){'.intval($n).'}/', $dirty_trash, $matches) > 0)
		{
			$newString = trim($matches[0]);
			if($matches[0] != $dirty_trash && $add_ellipses)
				$newString .= '...';
		}
		else
			$newString = trim($dirty_trash);
		
		return $newString;
	}
}

if(!function_exists('logged_query'))
{
	function logged_query($sql)
	{
		if(class_exists('Tabmin') && !empty(Tabmin::$db))
			$rs = mysql_query($sql, Tabmin::$db);
		else
			$rs = mysql_query($sql);
		if($rs)
		{
			QueryLog::add($sql);
			return $rs;
		}
		else
		{
			trigger_error('MySQL Error: '. mysql_error(), E_USER_ERROR);
			Console::add('Error SQL: '. $sql);
		}
		
		return false;
	}
}

if(!function_exists('pdologged_query'))
{
	function pdologged_query($sql)
	{
				try
		{
			$rs = Tabmin::$db->query($sql);
			if($rs)
			{
				//$row = $rs->fetch(PDO::FETCH_ASSOC);
				//var_dump($row);
				//die();
				QueryLog::add("pdo:\n\n".$sql);
				return $rs;
			}
			else
			{
				trigger_error('Programmers do not know what happend, but it is bad.', E_USER_ERROR);
				Console::add('Error SQL: '. $sql);
			}
		}
		catch (PDOException  $ex)
		{
			trigger_error('MySQL Error: '. $ex->getMessage(), E_USER_ERROR);
			Console::add('Error SQL: '. $sql);
		}
		
		
		return false;
	}
}

if(!function_exists('pdologged_exec'))
{
	function pdologged_exec($sql)
	{
		try
		{
			$count = Tabmin::$db->exec($sql);
			if($count >= 0)
			{				
				QueryLog::add("pdo-exec:\n\n".$sql);
				return $count;
			}			
		}
		catch (PDOException  $ex)
		{
			trigger_error('MySQL Error: '. $ex->getMessage(), E_USER_ERROR);
			Console::add('Error SQL: '. $sql);
		}
		
		
		return false;
	}
}

if(!function_exists('seconds_to_time'))
{
	function seconds_to_time($time)
	{
		if(is_numeric($time))
		{
			$value = array
			(
				'years' => 0, 
				'days' => 0, 
				'hours' => 0,
				'minutes' => 0, 
				'seconds' => 0
			);
			if($time >= 31556926)
			{
				$value['years'] = floor($time/31556926);
				$time = ($time % 31556926);
			}
			if($time >= 86400)
			{
				$value['days'] = floor($time/86400);
				$time = ($time % 86400);
			}
			if($time >= 3600)
			{
				$value['hours'] = floor($time/3600);
				$time = ($time % 3600);
			}
			if($time >= 60)
			{
				$value['minutes'] = floor($time/60);
				$time = ($time % 60);
			}
			
			$value['seconds'] = floor($time);
			return $value;
		}
		else
			return false;
	}
}

if(!function_exists('time_to_seconds'))
{
	function time_to_seconds($time)
	{
		$seconds = 0;
		$arr = explode(':', $time);
		Console::add(print_r($arr, true), print_r($time, true));
		$seconds = intval($arr[0]*60*60) + $arr[1]*60;
		if(!empty($arr[2])) //seconds
			$seconds += intval($arr[2]);
		
		return $seconds;
	}
}

if(!function_exists('relativeTime'))
{
	function relativeTime($time = false, $limit = 86400, $format = 'g:i A M jS') 
	{
		if (empty($time) || (!is_string($time) && !is_numeric($time))) 
			$time = time();
		elseif (is_string($time)) 
			$time = strtotime($time);
		
		$now = time();
		$relative = '';
		
		if ($time === $now) 
			$relative = 'now';
		elseif ($time > $now) 
			$relative = 'in the future';
		else 
		{
			$diff = $now - $time;
			
			if ($diff >= $limit) 
				$relative = date($format, $time);
			elseif ($diff < 60)
				$relative = 'less than one minute ago';
			elseif (($minutes = ceil($diff/60)) < 60)
				$relative = $minutes.' minute'.(((int)$minutes === 1) ? '' : 's').' ago';
			else 
			{
				$hours = ceil($diff/3600);
				$relative = 'about '.$hours.' hour'.(((int)$hours === 1) ? '' : 's').' ago';
			}
		}
		return $relative;
	}
}


// download from a URL to a destination
if( !function_exists('download') ) {
	function download($url, $destination) {
		try {
			$fp = fopen($destination, "w");
					if( !$fp )
					{
							echo 'fopen failed - Could not create a file at the $destination.<br />';
							return false;
					}
					$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, BB_USERNAME . ":" . BB_PASSWORD);
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			$resp = curl_exec($ch);
	
			// validate CURL status
			if(curl_errno($ch))
				throw new Exception(curl_error($ch), 500);
	
			// validate HTTP status code (user/password credential issues)
			$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if ($status_code != 200)
				throw new Exception("Response with Status Code [" . $status_code . "].<br />", 500);
		}
		catch(Exception $ex) {
			if ($ch != null) curl_close($ch);
			if ($fp != null) fclose($fp);
			throw new Exception('Unable to properly download file from url=[' + $url + '] to path [' + $destination + '].<br />', 500, $ex);
		}
		if ($ch != null) curl_close($ch);
		if ($fp != null) fclose($fp);
			
			return true;
	}
	
	// copy entire directory to new location
	function dir_copy_r($source, $destination) {
		try
			{
					$dir = opendir($source); 
					@mkdir($destination); 
					$to_delete = array();
					while(false !== ( $file = readdir($dir)) ) { 
							if (( $file != '.' ) && ( $file != '..' ) && 
									( $file != '.gitignore') && ( $file != 'README.md' ) && 
									( $file != '' )) { 
									if ( is_dir($source . '/' . $file) ) { 
											dir_copy_r($source . '/' . $file,$destination . '/' . $file); 
									} 
									else { 
											copy($source . '/' . $file,$destination . '/' . $file); 
									} 
							}
					} 
					closedir($dir);
			}
			catch(Exception $e)
			{
					return false;
			}
			
			return true;    
	}
	
	 // copy entire directory to new location
	function replace_config($source) {
			$str1 = "].'/../private/includes/config.inc.php');";
			$str2 = "].'/private/includes/config.inc.php');";
			$result = true;
		try
			{
					$dir = opendir($source);                
					while(false !== ( $file = readdir($dir)) ) { 
							if ( ( $file != '.' ) && ( $file != '..' ) )
							{ 
									if ( strpos($file, '.php') !== false )
									{
											$content = file_get_contents($source . '/' . $file);
											$content2 = str_replace($str1, $str2, $content);
											if (strlen($content) == strlen($content))
											$fp = @fopen($source . '/' . $file, 'w');
											if ($fp)
											{
													if (!fwrite($fp, $content2))                                            
															echo ('Failed to modifiy '. $source . '/' . $file);
											}
											else
											{
													echo ('No Write Permissisions on '. $source . '/' . $file);
													$result = false;
											}
											
											
											unset($content);
											unset($content2);
									}
									else if ( is_dir($source . '/' . $file) ) 
									{ 
											if (!replace_config($source . '/' . $file))
													$result = false;
									} 
							}
					} 
					closedir($dir);
			}
			catch(Exception $e)
			{
					return false;
			}
			
			return true;    
	}
}

//delete entire directory and all its contents
function dir_delete_r($dir) {
    try
        {
                $files = new RecursiveIteratorIterator(
                        new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
                        RecursiveIteratorIterator::CHILD_FIRST);

                foreach ($files as $fileinfo) {
                        $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
                        $todo($fileinfo->getRealPath());
                }
                
                rmdir($dir);
        }
        catch(Exception $e)
        {
                return false;
        }
        
        return true;    
} 

?>
