<?php
class AuditTrail extends Tobjects
{
    protected static $search_keys = array('');
    protected static $table_name = 'audit_trail';

	/***
	 * This function loads an object based on the parameters passed
	 * It requires parameters be defined in terms of column name, condition and value
	 ***/

	protected static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='')
	{
		if($where!='')
			$where='WHERE '.$where;

		if($order_by!='')
			$order_by='ORDER BY '.$order_by;

		$sql_calc_found_rows='';
		if($limit!='')
		{
			$limit='LIMIT '.$limit;
			$sql_calc_found_rows='SQL_CALC_FOUND_ROWS';
		}

		$sql=
			'SELECT DISTINCT '.$sql_calc_found_rows.'
				users.id AS users_id,
				users.username AS users_username,
				users.first_name AS users_first_name,
				users.last_name AS users_last_name,

				audit_trail.id,
				UNIX_TIMESTAMP(timestamp) AS timestamp,
				audit_types_id,
				message,
				remote_host,
				user_agent,
				url,
				git_hash
			FROM
				audit_trail
			LEFT JOIN users ON audit_trail.users_id = users.id
			'.$where.'
			'.$order_by.'
			'.$limit;

		self::$found_rows=0;
		$data=array();
		if($rs = pdologged_preparedQuery($sql, $values))
		{
			if(!empty($limit))
			{
				$rs_count = pdologged_query('SELECT FOUND_ROWS()');
				if($row = $rs_count->fetch(PDO::FETCH_NUM))
					self::$found_rows=$row[0];
			}
			else
			    self::$found_rows = $rs->rowCount();

			while($row = $rs->fetch(PDO::FETCH_ASSOC))
			{

				$log = new AuditTrailItem(array
				(
					'id'=>$row['id'],
					'timestamp'=>$row['timestamp'],
					'message'=>$row['message'],
					'remote_host'=>$row['remote_host'],
					'user_agent'=>$row['user_agent'],
					'url'=>$row['url'],
					'git_hash'=>$row['git_hash'],
					'user'=>array
					(
						'id'=>$row['users_id'],
						'first_name'=>$row['users_first_name'],
						'last_name'=>$row['users_last_name'],
						'username'=>$row['users_username']
					),
					'audit_type'=>array
                    (
                        'id'=>$row['audit_types_id']
                    ),

				));

				$data[] = $log;

				if(!$return_array)
					return $data[0];
			}

			return $data;
		}
		return false;
	}

	public static function log($user, $audit_types_id, $message)
	{
		$log = new AuditTrailItem(array
		(
			'message'=>$message,
			'remote_host'=>$_SERVER['REMOTE_ADDR'],
			'user_agent'=>$_SERVER['HTTP_USER_AGENT'],
			'url'=>$_SERVER['REQUEST_URI'],
			'user'=>array
			(
				'id'=>!empty($user) ? $user->getId() : null
			),
            'audit_type'=>array
            (
                'id'=>$audit_types_id
            )
		));
		$log->add();
	}

}

class AuditTrailItem extends Tobject
{
	protected $id, $user, $timestamp, $audit_type, $message, $remote_host, $user_agent, $url, $git_hash;

	function __construct($properties=NULL)
	{
		parent::__construct('audit_trail',$properties);
		if(isset($properties['user']))
			$this->user=new User($properties['user']);
		if(isset($properties['audit_type']))
			$this->audit_type=AuditTypes::loadById($properties['audit_type']['id']);
	}

    /**
     * @return \AuditType
     */
    public function getAuditType()
    {
        return $this->audit_type;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGitHash()
    {
        return $this->git_hash;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getRemoteHost()
    {
        return $this->remote_host;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return \User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }

    public function add()
    {
        $this->addRecord();
    }

	public function addRecord()
	{
		$users_id = null;
		if ($this->user)
			$users_id = $this->user->getId();

		$git_hash = 'LOCAL';
		if(class_exists('GitConfig'))
		{
			$git_hash = GitConfig::$TAG_NAME;
		}

		$sql =
			'INSERT INTO audit_trail
			(
				users_id,
				timestamp,
				audit_types_id,
				message,
				remote_host,
				user_agent,
				url,
				git_hash
			)
			VALUES
			(
				:users_id,
				NOW(),
				:audit_types_id,
				:message,
				:remote_host,
				:user_agent,
				:url,
				:git_hash
			)';

		$values = array(
			':users_id'=>$users_id,
			':audit_types_id'=>$this->audit_type->getId(),
			':message'=>$this->message,
			':remote_host'=>$this->remote_host,
			':user_agent'=>$this->user_agent,
			':url'=>$this->url,
			':git_hash'=>$git_hash
		);
		if(pdologged_preparedQuery($sql, $values))
		{
			$this->id=Tabmin::$db->lastInsertId();
			return true;
		}

		return false;
	}

	public function updateRecord()
	{
		return true;
	}

    public function update()
    {
        return true;
    }
}
?>