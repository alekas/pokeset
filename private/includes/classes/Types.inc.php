<?php
class Types extends Tobjects
{
    protected static $search_keys = array('');
    protected static $table_name = 'type';

    /***
     * This function loads an object based on the parameters passed
     * It requires parameters be defined in terms of column name, condition and value
     ***/

    protected static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='')
    {
        if($where!='')
            $where='WHERE '.$where;

        if($order_by!='')
            $order_by='ORDER BY '.$order_by;

        $sql_calc_found_rows='';
        if($limit!='')
        {
            $limit='LIMIT '.$limit;
            $sql_calc_found_rows='SQL_CALC_FOUND_ROWS';
        }

        $sql=
            'SELECT DISTINCT '.$sql_calc_found_rows.'
				id,
                name
			FROM
				types
			'.$where.'
			'.$order_by.'
			'.$limit;

        self::$found_rows=0;
        $data=array();
        if($rs = pdologged_preparedQuery($sql, $values))
        {
            if(!empty($limit))
            {
                $rs_count = pdologged_query('SELECT FOUND_ROWS()');
                if($row = $rs_count->fetch(PDO::FETCH_NUM))
                    self::$found_rows=$row[0];
            }
            else
                self::$found_rows = $rs->rowCount();

            while($row = $rs->fetch(PDO::FETCH_ASSOC))
            {

                $item = new Type(array
                (
                    'id'=>$row['id'],
                    'name'=>$row['name']
                ));

                $data[] = $item;

                if(!$return_array)
                    return $data[0];
            }

            return $data;
        }
        return false;
    }
}

class Type extends Tobject
{
    protected $id, $name;

    function __construct($properties=NULL)
    {
        parent::__construct('type',$properties);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function add()
    {
        $sql =
            'INSERT INTO types
            (
                name
            )
            VALUES
            (
                :name
            )';

        $values = array(
            ':name'=>$this->name
        );
        if(pdologged_preparedQuery($sql, $values))
        {
            $this->id=Tabmin::$db->lastInsertId();
            return true;
        }

        return false;
    }

    public function update()
    {
        return true;
    }
}
?>