<?php
class PokemonCollection extends Tobjects
{
    protected static $search_keys = array('');
    protected static $table_name = 'pokemon';

    /***
     * This function loads an object based on the parameters passed
     * It requires parameters be defined in terms of column name, condition and value
     ***/

    protected static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='')
    {
        if($where!='')
            $where='WHERE '.$where;

        if($order_by!='')
            $order_by='ORDER BY '.$order_by;

        $sql_calc_found_rows='';
        if($limit!='')
        {
            $limit='LIMIT '.$limit;
            $sql_calc_found_rows='SQL_CALC_FOUND_ROWS';
        }

        $sql=
            'SELECT DISTINCT '.$sql_calc_found_rows.'
				id,
                number,
                hp,
                attack,
                defense,
                special_attack,
                special_defense,
                speed,
                pretty_name,
                name
			FROM
				pokemon
			'.$where.'
			'.$order_by.'
			'.$limit;

        self::$found_rows=0;
        $data=array();
        if($rs = pdologged_preparedQuery($sql, $values))
        {
            if(!empty($limit))
            {
                $rs_count = pdologged_query('SELECT FOUND_ROWS()');
                if($row = $rs_count->fetch(PDO::FETCH_NUM))
                    self::$found_rows=$row[0];
            }
            else
                self::$found_rows = $rs->rowCount();

            while($row = $rs->fetch(PDO::FETCH_ASSOC))
            {

                $item = new Pokemon(array
                (
                    'id'=>$row['id'],
                    'number'=>$row['number'],
                    'hp'=>$row['hp'],
                    'attack'=>$row['attack'],
                    'defense'=>$row['defense'],
                    'special_attack'=>$row['special_attack'],
                    'special_defense'=>$row['special_defense'],
                    'speed'=>$row['speed'],
                    'pretty_name'=>$row['pretty_name'],
                    'name'=>$row['name']
                ));

                $data[] = $item;

                if(!$return_array)
                    return $data[0];
            }

            return $data;
        }
        return false;
    }
}

class Pokemon extends Tobject
{
    protected $id, $number, $hp, $attack, $defense, $special_attack;
    protected $special_defense, $speed, $name, $pretty_name, $types;

    function __construct($properties=NULL)
    {
        parent::__construct('pokemon',$properties);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAttack()
    {
        return $this->attack;
    }

    /**
     * @return mixed
     */
    public function getDefense()
    {
        return $this->defense;
    }

    /**
     * @return mixed
     */
    public function getHp()
    {
        return $this->hp;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSprite()
    {
        return strtolower($this->name).'.png';
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function getPrettyName()
    {
        return $this->pretty_name;
    }

    /**
     * @return mixed
     */
    public function getSpecialAttack()
    {
        return $this->special_attack;
    }

    /**
     * @return mixed
     */
    public function getSpecialDefense()
    {
        return $this->special_defense;
    }

    /**
     * @return mixed
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @return mixed
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param mixed $types
     */
    public function setTypes($types)
    {
        $this->types = $types;
    }

    public function add()
    {
        $sql =
            'INSERT INTO pokemon
            (
                number,
                hp,
                attack,
                defense,
                special_attack,
                special_defense,
                speed,
                pretty_name,
                name
            )
            VALUES
            (
                :number,
                :hp,
                :attack,
                :defense,
                :special_attack,
                :special_defense,
                :speed,
                :pretty_name,
                :name
            )';

        $values = array(
            ':number'=>$this->number,
            ':hp'=>$this->hp,
            ':attack'=>$this->attack,
            ':defense'=>$this->defense,
            ':special_attack'=>$this->special_attack,
            ':special_defense'=>$this->special_defense,
            ':speed'=>$this->speed,
            ':pretty_name'=>$this->pretty_name,
            ':name'=>$this->name
        );
        if(pdologged_preparedQuery($sql, $values))
        {
            $this->id=Tabmin::$db->lastInsertId();
            if(!empty($this->types))
            {
                $sql = 'INSERT INTO pokemon_to_types (pokemon_id, types_id) VALUES ';
                $values = array();
                foreach($this->types as $type)
                {
                    if(!empty($type->getId()))
                    {
                        $values[] = '('.intval($this->getId()).', '.intval($type->getId()).')';
                    }
                }

                if(!empty($values))
                {
                    $sql .= implode(', ', $values);
                    pdologged_exec($sql);
                }
            }
            return true;
        }

        return false;
    }

    public function update()
    {
        return true;
    }
}
?>