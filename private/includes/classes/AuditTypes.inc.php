<?
class AuditTypes
{
    protected static $data;

    const LOGIN=1;
    const LOGOUT=2;
    const RECORD_ADD=3;
    const RECORD_EDIT=4;
    const RECORD_DELETE=5;

    public static function loadAll()
    {
        if(!isset(self::$data))
            self::lazyLoad();

        return self::$data;
    }

    public static function loadById($id)
    {
        if(!isset(self::$data))
            self::lazyLoad();

        for($i=0; $i < count(self::$data); $i++)
        {
            if(self::$data[$i]->getId()==$id)
                return self::$data[$i];
        }

        return false;
    }

    protected static function lazyLoad()
    {
        self::$data=array
        (
            new AuditType(array
            (
                'id'=>self::LOGIN,
                'name'=>'Login',
            )),
            new AuditType(array
            (
                'id'=>self::LOGOUT,
                'name'=>'Logout',
            )),
            new AuditType(array
            (
                'id'=>self::RECORD_ADD,
                'name'=>'Record Added',
            )),
            new AuditType(array
            (
                'id'=>self::RECORD_EDIT,
                'name'=>'Record Edited',
            )),
            new AuditType(array
            (
                'id'=>self::RECORD_DELETE,
                'name'=>'Record Deleted',
            ))
        );
    }
}

class AuditType
{
    protected $id, $name;

    function __construct($properties)
    {
        foreach($properties as $property=>$value)
        {
            if(property_exists($this, $property))
                $this->{"$property"}=$value;
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


}
?>
