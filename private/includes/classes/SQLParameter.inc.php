<?php
class SQLParameter
{
	private $column, $value, $condition, $type, $value2;
	private $values, $stub;
	private $params;
	private $op;

	/**
	 * Function: Constructor
	 *
	 * 		This function cretes SQLParameter Object
	 *
	 * Parameters:
	 * 		$column - the column of the tabel
	 * 		$condition - condition. Possible values are '=', '<', '>', '>=','<=','is_null', 'is_not_null', 'BETWEEN'
	 * 		$value - the value used in comparison
	 * 		$type - type of variable, can be int or string
	 * 		$value2 - value2 for the between clause
	 *
	 * Returns:
	 * 		uncompiled SQLParameter object
	 */
	public function __construct($column = false, $condition = false, $value = false, $type = 'string', $value2 = '')
	{
		if ($column && $condition)
		{
			$this->column = $column;
			$this->value = $value;
			$this->condition = $condition;
			$this->type = $type;
			$this->value2 = $value2;
		}
		$this->values = array();
	}

	/**
	 * Function setCompiled
	 * 		sets the stub and values to act as Compiled SQLParameter
	 *
	 * Parameters:
	 * 		$compiled - stub
	 * 		$values - array values
	 *
	 */
	public function setCompiled($stub, $values)
	{
		$this->stub = $stub;
		$this->values = $values;
	}

	public function setOperator($op)
	{
		$this->op = $op;
	}

	public function getOperator()
	{
		return $this->op;
	}

	/**
	 * Function addAND
	 * 		ANDs a SQLParameter to this parameter and compiles it into a new SQLParameter
	 *
	 * Parameters:
	 * 		$sqlParam - SQLParameter to AND
	 *
	 * Returns:
	 * 		SQLParameter
	 */
	public function addAND($sqlParam)
	{
		if ($this->params == null)
		{
			$this->params = array();
		}
		$this->params[] = $sqlParam;
		$sqlParam->setOperator("AND");
		return $this->compile();
	}

	/**
	 * Function addOR
	 * 		ORs a SQLParameter to this parameter and compiles it into a new SQLParameter
	 *
	 * Parameters:
	 * 		$sqlParam - SQLParameter to OR
	 *
	 * Returns:
	 * 		SQLParameter
	 *
	 */
	public function addOR($sqlParam)
	{
		if ($this->params == null)
		{
			$this->params = array();
		}
		$this->params[] = $sqlParam;
		$sqlParam->setOperator("OR");
		return $this->compile();
	}

	/**
	 * Function getValues
	 * 		gets values form this SQLParameter
	 *
	 *
	 * Returns:
	 * 		array of values
	 */
	public function getValues()
	{
		if (count($this->values) > 0 || ($this->stub))
			return $this->values;
		else
		{
			$this->compile();
			return $this->values;
		}
	}

	/**
	 * Function getStub
	 * 		get a stub form this SQLParameter
	 *
	 *
	 *
	 * Returns:
	 * 		stub
	 */
	public function getStub()
	{
		if ($this->stub)
			return $this->stub;
		else
		{
			$this->compile();
			return $this->stub;
		}
	}

	public function createThisStubAndValues()
	{
		$pname = 'p'.randString(3);
		$pname .= $this->column;
		$pname = str_replace('.','',$pname);

		if ($this->stub == '')
		{
			if ($this->condition == 'is_null')
			{
				$this->stub = $this->column.' is NULL';
			}
			else if ($this->condition == 'is_not_null')
			{
				$this->stub = $this->column.' is NOT NULL';
			}
			else if ($this->condition != 'BETWEEN')
			{
				$this->stub = $this->column.$this->condition.' :'.$pname;
				if ($this->type == 'int')
					$this->values[':'.$pname] = intval($this->value);
				else
					$this->values[':'.$pname] = $this->value;
			}
			else
			{
				switch(strtoupper($this->condition))
				{
					case 'BETWEEN':
						$this->stub = $this->column.' '.$this->condition.' :'.$pname.'1 AND :'.$pname.'2';
						$this->values[':'.$pname.'1'] = $this->value;
						$this->values[':'.$pname.'2'] = $this->value2;
						break;
				}
			}
		}
	}

	/**
	 * Function getValues
	 * 		compiles this SQLParameter into a stub, values combination.
	 * 		this function has a side-effect, as this SQLParameter becomes compiled,
	 * 		thus loosing its original state.
	 *
	 * Returns:
	 * 		compiled SQLParameter
	 */
	public function compile()
	{
		$this->createThisStubAndValues();
		if (count($this->params) > 0 )
		{
			$this->stub = ' ( '.$this->stub ;
			foreach ($this->params as $param)
			{
				$param->compile();//compile to get stub and values
				$this->stub .= ' '.$param->getOperator(). ' ' . $param->getStub();//use stub and values
				$tempVals  = $param->getValues();
				if (count($tempVals) > 0)
				{
					$this->values = array_merge($this->values, $tempVals);
				}
			}
			$this->stub .= ' ) ';
		}
		$this->params = array();
		$rp = new SQLParameter();
		$rp->setCompiled($this->stub, $this->values);
		return $this;
	}
}
?>