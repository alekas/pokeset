<?php
abstract class Tobjects
{
	protected static $found_rows=0;
	protected static $search_keys;
	protected static $table_name = '';

	/*
	 * Function: loadById
	 *
	 * 		loads tobject by its id
	 * Parameters:
	 * 		$id - the id of the tobject
	 *
	 * Returns:
	 * 		tobject or false if no tobject exists with such id,
	 */
	public static function loadById($id)
	{
		if (!static::$table_name || static::$table_name == '')
			trigger_error('Table name is not defined',  E_USER_ERROR);
		$where=static::$table_name.'.id= :id';
		$values[':id'] = intval($id);
		return static::load($where, $values);
	}

	/*
	 * Function: loadByIds
	 *
	 * 		loads tobject by its id
	 * Parameters:
	 * 		$ids - array of ids for the tobjects to be loaded
	 * 		$order_by - ordering order. must be a SQL ORDER BY statement
	 * 		$limit - SQL LIMIT
	 *
	 * Returns:
	 * 		an array of tobjects or false if no tobjects exists with given ids,
	 */
	public static function loadByIds($ids, $order_by='', $limit='')
	{
		if (!static::$table_name || static::$table_name == '')
			trigger_error('Table name is not defined',  E_USER_ERROR);
		for($i=0; $i<count($ids); $i++)
			$ids[$i]=intval($ids[$i]);

		$where=static::$table_name.'.id IN (';
		foreach ($ids as $i=>$id)
		{
			$where.=':id'.$i . ((count($ids) - 1) > $i ? ',' : '');
			$values[':id'.$i] = $id;
		}
		$where .= ')';

		return static::load($where, $values, true, $order_by, $limit);
	}




	/*
	 * Function: loadByParameters
	 *
	 * 		This function loads an object based on the parameters passed
	 * 		It requires parameters be defined in terms of column name, condition and value
	 * Parameters:
	 * 		$parameters - array of parameter arrays such. Each parameter array defines the data type of the parameter, comparison condition, and value
	 * 			$parameters['table.column'] = array('type'=>'data_type', 'condition'=>'=', 'value'=>'value');
	 * 			data_type can be int or string
	 * 			comparison conditions can be any valid SQL comparison operator or the keyword 'BETWEEN'
	 * 			in case the 'BETWEEN' is used, 'value' needs to be omitted and 'value1' and 'value2' used instead.
	 * 			optional 'andOrOperator' can be specified in order to change how parameters are treated (if OR/AMD is placed between two parameters)
	 * 		$order_by - ordering order. must be a SQL ORDER BY statement
	 * 		$limit - SQL LIMIT
	 *
	 * Returns:
	 * 		an array of tobjects or false if no tobjects exists
	 */
	public static function loadByParameters($parameters, $order_by='', $limit='')
	{
		if (($parameters instanceof SQLParameter))
		{
			$whereStr = $parameters->getStub();
			$values = $parameters->getValues();
		}
		else
		{
			$whereStr = '';
			$pwhere = static::getParamtersWhere($parameters);
			if ($pwhere)
			{
				$where = $pwhere[0];
				$values = $pwhere[1];
				$andOrOp = $pwhere[2];
				foreach($where as $i=>$where_tmp)
				{
					$whereStr .= ($i > 0 ? ' '.$andOrOp[$i].' ' : '').$where[$i];
				}

				//$whereStr = implode(' AND ', $where);
			}
		}



		return static::load($whereStr, $values, true, $order_by, $limit);

	}

	protected static function getParamtersWhere($parameters)
	{
		$where = array();
		$values = array();
		$andOrOperators = array();
		$ret = array();
		if ($parameters)
		{
			foreach($parameters as $parameter=>$value)
			{
				$pname = str_replace('.','',$parameter);
				if (!empty($value['andOrOperator']))
					$andOrOperators[] = addslashes($value['andOrOperator']);
				else
					$andOrOperators[] = 'AND';
				if ($value['condition'] == 'is_null')
				{
					$where[] = $parameter.' is NULL';
				}
				else if ($value['condition'] != 'Between')
				{
					$where[] = $parameter.$value['condition'].' :'.$pname;
					if ($value['type'] == 'int')
						$values[':'.$pname] = intval($value['value']);
					else
						$values[':'.$pname] = $value['value'];
				}
				else
				{
					switch(strtoupper($value['condition']))
					{
						case 'BETWEEN':
							$where[] = $parameter.' '.$value['condition'].' :'.$pname.'1 AND :'.$pname.'2';
							$values[':'.$pname.'1'] = $value['value1'];
							$values[':'.$pname.'2'] = $value['value2'];
							break;
					}
				}
			}
			$ret[] = $where;
			$ret[] = $values;
			$ret[] = $andOrOperators;
		}
		else
			$ret = false;
		return $ret;
	}

	/*
	 * Function: searchLoad
	 *
	 * 		This function performs search on the specified columns and loads Users that match the search result
	 * Parameters:
	 * 		$search - string to search for in the searchable columns
	 * 		$order_by - ordering order. must be a SQL ORDER BY statement
	 * 		$limit - SQL LIMIT
	 *
	 * Returns:
	 * 		an array of tobjects or false if no tobjects exists
	 */
	public static function searchLoad($search, $order_by='', $limit='')
	{
		$where = '';
		$pwhere = static::getSearchLoadWhere($search);
		if ($pwhere)
		{
			$where = $pwhere[0];
			$values = $pwhere[1];
		}
		return static::load($where, $values, true, $order_by, $limit);
	}

	protected static function getSearchLoadWhere($search)
	{
		$where = '';
		$values = array();
		$ret = array();
		if(!empty($search))
		{
			$search_sql=array();
			$search_terms=preg_split('/\s+/', trim($search));
			$counter = 0;
			for($i=0; $i<count($search_terms); $i++)
			{
				foreach (static::$search_keys as $search_key)
				{
					$search_sql[]=$search_key.' LIKE :search'.$counter;
					$values[':search'.$counter] = '%'. $search_terms[$i].'%';
					$counter++;
				}
			}
			$where='('. implode(' OR ', $search_sql) .')';
			$ret[] = $where;
			$ret[] = $values;
		}
		else
			$ret = false;

		return $ret;
	}

	/*
	 * Function: searchLoadByParameters
	 *
	 * 		This function performs search fo a given string on the specified columns and applies parameter to load theUsers that match the search criteria and parameter
	 *
	 * Parameters:
	 * 		$search - string to search for in the searchable columns
	 * 		$parameters - array of parameter arrays such. Each parameter array defines the data type of the parameter, comparison condition, and value
	 * 			$parameters['table.column'] = array('type'=>'data_type', 'condition'=>'=', 'value'=>'value');
	 * 			data_type can be int or string
	 * 			comparison conditions can be any valid SQL comparison operator or the keyword 'BETWEEN'
	 * 			in case the 'BETWEEN' is used, 'value' needs to be omitted and 'value1' and 'value2' used instead.
	 * 			optional 'andOrOperator' can be specified in order to change how parameters are treated (if OR/AMD is placed between two parameters)
	 * 		$order_by - ordering order. must be a SQL ORDER BY statement
	 * 		$limit - SQL LIMIT
	 *
	 * Returns:
	 * 		an array of tobjects or false if no tobjects exists
	 */
	public static function searchLoadByParameters($search, $parameters= NULL, $order_by='', $limit='')
	{
		$svalues = array();
		$pvalues = array();
		$pwhereStub = array();
		$swhereStub = false;
		if ($parameters)
		{
			if (($parameters instanceof SQLParameter))
			{
				$pwhereStub = $parameters->getStub();
				$pvalues = $parameters->getValues();
			}
			else
			{
				$pwhere = static::getParamtersWhere($parameters);

				if ($pwhere)
				{
					$pwhereStub = $pwhere[0];
					$pvalues = $pwhere[1];
				}
			}
		}

		$swhere = static::getSearchLoadWhere($search);
		if ($swhere)
		{
			$swhereStub = $swhere[0];
			$svalues = $swhere[1];
		}
		$values = array_merge($pvalues, $svalues);
		if (is_array($pwhereStub))
			$where = implode(' AND ', $pwhereStub);
		else
			$where = $pwhereStub;
		if ($swhereStub)
			$where .= (strlen($where) > 0 ? ' AND ' : '') .$swhereStub;
		return static::load($where, $values, true, $order_by, $limit);
	}

	/*
	 * Function: loadAll
	 *
	 * 		This function loads all tobjects from he database
	 *
	 * Parameters:	 *
	 * 		$order_by - ordering order. must be a SQL ORDER BY statement
	 * 		$limit - SQL LIMIT
	 *
	 * Returns:
	 * 		an array of tobjects or false if no tobjects exists
	 */
	public static function loadAll($order_by='', $limit='')
	{
		return static::load('',NULL, true, $order_by, $limit);
	}

	protected abstract static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='');

	/*
	 * Function: getFoundRows
	 *
	 * 		This function gets the number of rows returned in the most recent tobjects load
	 *
	 * Returns:
	 * 		integer number of rows returned in the most recent tobjects load
	 */
	public static function getFoundRows()
	{
		return static::$found_rows;
	}
}

abstract class Tobject
{
	private $table_name;
	protected $inclusion;

	function __construct($table_name, $properties=NULL)
	{
		$this->inclusion = array();
		$this->table_name = $table_name;
		if( count($properties) >= 1 )
		{
			foreach($properties as $property=>$value)
			{
				if(property_exists($this, $property))
				{
					$this->{"$property"}=$value;
					$this->inclusion[]=$property;
				}
			}
		}
	}

    public abstract function getId();
    public abstract function add();
    public abstract function update();

	public function generateSimpleUpdateFields($exclude=array())
	{
		$sql = '';
		$values = array();
		$incCount = count($this->inclusion);
		foreach($this->inclusion as $i=>$property)
		{
			if ((!is_object($this->{$property})) && (!in_array($property,$exclude)))
			{
				if ($incCount == $i+1)
				{
					$comma ='
					';
				}
				else
				{
					$comma =',
					';
				}

				$sql .= $this->table_name.'.'.$property . '= :'.$this->table_name.$property.$comma;
				if(is_int($this->{$property}))
					$values[':'.$this->table_name.$property] = intval($this->{$property});
				else
					$values[':'.$this->table_name.$property] = $this->{$property};
			}
		}
		$sql = trim($sql);	//find a comma in case we still put it... quick and dirty fix
		if (substr($sql,strlen($sql)-1,1) == ',')
			$sql = substr_replace($sql,'',strlen($sql)-1,1);
		return new stubContainer($sql, $values);
	}

	public function delete()
	{
        global $currentUser;
		$sql =
			'DELETE FROM
				'.$this->table_name.'
			WHERE
				id='.intval($this->id);
		if(pdologged_exec($sql) !== false)
		{
            AuditTrail::log($currentUser, AuditTypes::RECORD_DELETE,
                'Record deleted from table "'.addslashes($this->table_name).'". ID: '.intval($this->getId()));
			return true;
		}
		return false;

	}
}

class stubContainer
{
	private $stub, $values;
	function __construct($stub, $values)
	{
		$this->stub = $stub;
		$this->values = $values;
	}

	public function getStub()
	{
		return $this->stub;
	}

	public function getValues()
	{
		return $this->values;
	}
}
?>