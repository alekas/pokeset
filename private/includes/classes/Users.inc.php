<?php
class Users extends Tobjects
{
	
	protected static $search_keys = array('users.first_name', 'users.last_name');
	protected static $table_name = 'users';
	
	public static function loadAllWithHidden($order_by='', $limit='')
	{
		//$parameters['roles.hidden'] = array('type'=>'int', 'condition'=>'=', 'value'=>0);
		$parameters = array();
		return parent::loadByParameters($parameters, $order_by, $limit);
	}

	public static function loadBySAMAccountName($sam_account_name)
	{
        return self::load('sam_account_name=:sam_account_name', array(':sam_account_name'=>$sam_account_name));
	}
	
	public static function loadAll($order_by='', $limit='')
	{
		$parameters['roles.hidden'] = array('type'=>'int', 'condition'=>'=', 'value'=>0); 
		return parent::loadByParameters($parameters, $order_by, $limit);
	}

	/***
	 * This function loads an object based on the parameters passed
	 * It requires parameters be defined in terms of column name, condition and value
	***/
	public static function loadByParameters($parameters, $order_by='', $limit='')
	{
		$parameters['roles.hidden'] = array('type'=>'int', 'condition'=>'=', 'value'=>0); 
		return parent::loadByParameters($parameters, $order_by, $limit);
	}
	
	
		
	/***
	 * This function performs search on the specified columns and loads Users that match teh search result
	***/	
	public static function searchLoad($search, $order_by='', $limit='')
	{		
		$where = array();
		$where[] = self::getSearchLoadWhere($search);
		$where[] = 'roles.hidden=FALSE';
		return self::load(implode(' AND ', $where), true, $order_by, $limit);
	}
		
	public static function searchLoadByParameters($search, $parameters, $order_by='', $limit='')
	{		
		$parameters['roles.hidden'] = array('type'=>'int', 'condition'=>'=', 'value'=>'FALSE'); 
		return parent::searchLoadByParameters($search, $parameters, $order_by, $limit);
	}
		
	protected static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='')
	{
		if($where!='')
			$where='WHERE '.$where;
		
		if($order_by!='')
			$order_by='ORDER BY '.$order_by;
		
		$sql_calc_found_rows='';
		if($limit!='')
		{
			$limit='LIMIT '.$limit;
			$sql_calc_found_rows='SQL_CALC_FOUND_ROWS';
		}
		
		$sql=
			'SELECT DISTINCT '.$sql_calc_found_rows.'
				users.id,
				email,
				sam_account_name,
				first_name,
				last_name,
				phone,
				UNIX_TIMESTAMP(last_login) AS last_login,
				UNIX_TIMESTAMP(created_timestamp) AS created_timestamp,

				roles.id AS roles_id,
				roles.hidden AS roles_hidden,
				roles.developer AS roles_developer,
				roles.role AS roles_role
			FROM
				users
			LEFT JOIN roles ON users.roles_id=roles.id
			'.$where.'
			'.$order_by.'
			'.$limit;
		
		self::$found_rows=0;
		$data=array();
		if($rs = pdologged_preparedQuery($sql, $values))
		{
			if(!empty($limit))
			{
				$rs_count = pdologged_query('SELECT FOUND_ROWS()');
				if($row = $rs_count->fetch(PDO::FETCH_NUM))
					self::$found_rows=$row[0];
			}
			else
				self::$found_rows = $rs->rowCount();
			
			while($row = $rs->fetch(PDO::FETCH_ASSOC))
			{			

				$user = new User(array
				(
					'id'=>$row['id'],
					'email'=>$row['email'],
					'sam_account_name'=>$row['sam_account_name'],
					'first_name'=>$row['first_name'],
					'last_name'=>$row['last_name'],
					'phone'=>$row['phone'],		
					'last_login'=>$row['last_login'],					
					'created_timestamp'=>$row['created_timestamp'],
					'role'=>array
					(
						'id'=>$row['roles_id'],
						'hidden'=>$row['roles_hidden'],
						'developer'=>$row['roles_developer'],
						'role'=>$row['roles_role']
					)
				));
				
				
				$data[] = $user;
				
				if(!$return_array)
					return $data[0];
			}			
			
			return $data;
		}
		return false;
	}
		
	public static function exists($email, $users_id_ignored='')
	{
		$sql=
			'SELECT
				users.id
			FROM
				users
			WHERE
			(
				TRIM(email)=TRIM("'. addslashes($email) .'")
			)';
		if(!empty($users_id_ignored))
			$sql.=' AND id<>'. intval($users_id_ignored);
		
		if($rs=pdologged_query($sql))
		{
			if($row=$rs->fetch(PDO::FETCH_ASSOC))
				return $row['id'];
			return false;
		}
		
		return true;
	}
	
	public static function login($username, $password)
	{
        $ldap = new LDAP(ACTIVE_DIRECTORY_HOST_NAME);
        $ldap->setDn(ACTIVE_DIRECTORY_DN);
        if($ldap->authenticate($username, $password))
        {
            $info = $ldap->getUserInformation();
            if($info)
            {
                $user = (new Users())->loadBySAMAccountName($username);
                $roles_id = ROLES_USER;
                Console::add($info);
                if($info['ou']==ACTIVE_DIRECTORY_ADMIN_OU)
                {
                    $roles_id = ROLES_ADMIN;
                }
                $user = new User(array
                (
                    'id'=>$user ? $user->getId() : null,
                    'email'=>$info['email'],
                    'sam_account_name'=>$username,
                    'password'=>$password,
                    'first_name'=>$info['first_name'],
                    'last_name'=>$info['last_name'],
                    'phone'=>$info['phone'],
                    'role'=>array
                    (
                        'id'=>$roles_id
                    )
                ));

                if($user) //update current user with LDAP info that may have changed
                {
                    $user->update();
                }
                else //add new user
                {
                    $user->add();
                }
                AuditTrail::log($user, AuditTypes::LOGIN, 'User logged in');
                $user->updateLastLogin();
                return $user;
            }
        }
        else
        {
            $where=
                '(
                    email=:username
                    OR sam_account_name=:username2
                )
                AND
                password=SHA1(CONCAT(:password, salt))';

            $values = array(
                ':username'=>$username,
                ':username2'=>$username,
                ':password'=>$password
            );

            if($user=self::load($where, $values, false))
            {
                AuditTrail::log($user, AuditTypes::LOGIN, 'User logged in');
                $user->updateLastLogin();
                return $user;
            }
        }
		return false;
	}
	
	public static function sendForgotPasswordLink($email)
	{
		if($id = self::exists($email))
		{
			if(is_numeric($id))
			{
				$key = randString(20);
				$sql =
					'UPDATE users SET
						password_retrieval_key = "'.addslashes($key).'"
					WHERE 
						id='.$id.'
					LIMIT 1';
				if($rs=pdologged_query($sql))
				{
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: website@website.com'."\r\n";
					
					$html = 'This email has been sent to you automatically in order to change your password. To continue the process, please click the below link. <br><br><a href="http://site.elykinnovation.com/forgot-pw.php?key='.$key.'">http://site.elykinnovation.com/forgot-pw.php?key='.$key.'</a>';
					mail($email, 'Site Password Recovery', $html, $headers);
					return true;
				}
			}
		}
		return false;
	}
	
	public static function setPassword($key, $password)
	{
		$salt=randString(10);
		$sql = 
			'UPDATE users SET
				password = SHA1(CONCAT("'.addslashes($password).'", "'. addslashes($salt) .'")),
				salt="'. addslashes($salt) .'",
				password_retrieval_key = NULL
			WHERE
				password_retrieval_key="'.addslashes($key).'"
			LIMIT 1';
		if($rs=pdologged_exec($sql))
		{
			if($rs > 0)
				return true;
		}
		
		return false;
	}
}

class User extends Tobject
{
	protected $id, $email, $sam_account_name, $password, $phone;
	protected $role;
	protected $last_name, $first_name, $last_login, $created_timestamp;
	
	function __construct($properties=NULL)
	{		
		parent::__construct('users',$properties);
		if(isset($properties['role']))
			$this->role=new Role($properties['role']);
	}
	
	public function getId()
	{
		return intval($this->id);
	}
	
	public function getRole()
	{
		return ($this->role);
	}

	public function getEmail()
	{
		return ($this->email);
	}

	public function getSAMAccountName()
	{
		return ($this->sam_account_name);
	}
	
	public function getFirstName()
	{
		return ($this->first_name);
	}
	
	public function getLastName()
	{
		return ($this->last_name);
	}

    public function getPhone()
    {
        return $this->phone;
    }


	public function getLastLogin()
	{
		return $this->last_login;
	}

    /**
     * @return int UNIX timestamp that the record was created
     */
    public function getCreatedTimestamp()
    {
        return $this->created_timestamp;
    }
	
	public function setLastLogin($last_login)
	{
		$this->last_login = $last_login;
	}
	
	
	public function addRecord()
	{
		$salt = randString(10);
		$roles_id='DEFAULT';
		if(!empty($this->role))//we check if object exists, but we might also need to check if id > 0
			$roles_id=$this->role->getId();
			
		$sql =
			'INSERT INTO users
			(
				roles_id,
				email,
				sam_account_name,
				password,
				salt,
				first_name,
				last_name,
				phone
			)
			VALUES
			(
				:roles_id,
				:email,
				:sam_account_name,
				SHA(:password),
				:salt,
				:first_name,
				:last_name,
				:phone
			)';
			
		$values = array(
			':roles_id'=>$roles_id,
			':email'=>($this->email),
			':sam_account_name'=>($this->sam_account_name),
			':password'=>$this->password.$salt,
			':salt'=>$salt,
			':first_name'=>$this->first_name,
			':last_name'=>$this->last_name,
			':phone'=>$this->phone
		);
		if(pdologged_preparedQuery($sql, $values))
		{
			$this->id=Tabmin::$db->lastInsertId();
			return true;
		}
		
		return false;
	}
	
	public function updateRecord()
	{
		$sql_simpleFields = $this->generateSimpleUpdateFields(array('password', 'created_timestamp'));
		
		$roles_id='';
		if(!empty($this->role))//we check if object exists, but we might also need to check if id > 0
			$roles_id='roles_id='.intval($this->role->getId()).',';
		
		$password='';
		$salt='';
		if(!empty($this->password))
		{
			$salt=addslashes(randString(10));
			$password='password=SHA1(CONCAT("'.addslashes($this->password).'", "'. addslashes($salt) .'")),';
			$salt='salt="'. $salt .'",';
		}
		
		$sql =
			'UPDATE users SET
				'.$roles_id.'
				'.$password.'
				'.$salt.'
				'.$sql_simpleFields->getStub().'
			WHERE id=:id';
		
		$values = $sql_simpleFields->getValues();
		$values[':id'] = $this->getId();
		//AlertSet::addError(var_export($sql, true));
		if(pdologged_preparedQuery($sql, $values) !== false)
			return true;
		return false;
	}
		
	public function updateLastLogin()
	{
		$this->last_login=date('Y-m-d h:i:s');
		$sql =
			'UPDATE users SET
				last_login= :lastlogin
			WHERE id=:id';
		
		$values = array(
			':lastlogin'=>date('Y-m-d h:i:s'),
			':id'=>intval($this->id)
		);
		if(pdologged_preparedQuery($sql, $values) !== false)
        {
            return true;
        }
		return false;
	}
	
	public function hasPermission($module, $verb, $own=NULL)
	{
		return Permissions::hasPermission($module, $verb, $own);
	}
	
	public function tabPermission($module, $tab)
	{
		return Permissions::tabPermission($module, $tab);
	}
}
?>