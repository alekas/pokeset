<?php
class Backtraces extends Tobjects
{
	protected static $search_keys = array('backtraces.first_name', 'backtraces.last_name');
	protected static $table_name = 'backtraces';

	public static function loadAll($order_by='', $limit='')
	{
		return parent::loadByParameters('', $order_by, $limit);
	}

	/***
	 * This function loads an object based on the parameters passed
	 * It requires parameters be defined in terms of column name, condition and value
	***/
	public static function loadByParameters($parameters, $order_by='', $limit='')
	{
		return parent::loadByParameters($parameters, $order_by, $limit);
	}
		
	/***
	 * This function performs search on the specified columns and loads Backtraces that match teh search result
	***/	
	public static function searchLoad($search, $order_by='', $limit='')
	{		
		$where = array();
		$where[] = self::getSearchLoadWhere($search);
		return self::load(implode(' AND ', $where), true, $order_by, $limit);
	}
		
	public static function searchLoadByParameters($search, $parameters, $order_by='', $limit='')
	{
		return parent::searchLoadByParameters($search, $parameters, $order_by, $limit);
	}
		
	protected static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='')
	{
		if($where!='')
			$where='WHERE '.$where;
		
		if($order_by!='')
			$order_by='ORDER BY '.$order_by;
		
		$sql_calc_found_rows='';
		if($limit!='')
		{
			$limit='LIMIT '.$limit;
			$sql_calc_found_rows='SQL_CALC_FOUND_ROWS';
		}
		
		$sql=
			'SELECT DISTINCT '.$sql_calc_found_rows.'
				backtraces.id,
				backtrace.errors_id,
                type,
                function
                class,
                line,
                file
			FROM
				backtraces
			'.$where.'
			'.$order_by.'
			'.$limit;
		
		self::$found_rows=0;
		$data=array();
		if($rs = pdologged_preparedQuery($sql, $values))
		{
			if(!empty($limit))
			{
				$rs_count = pdologged_query('SELECT FOUND_ROWS()');
				if($row = $rs_count->fetch(PDO::FETCH_NUM))
					self::$found_rows=$row[0];
			}
			else
				self::$found_rows = $rs->rowCount();
			
			while($row = $rs->fetch(PDO::FETCH_ASSOC))
			{			

				$backtrace = new Backtrace(array
				(
                    'id'=>$row['id'],
                    'type'=>$row['type'],
                    'function'=>$row['function'],
                    'class'=>$row['class'],
                    'line'=>$row['line'],
                    'file'=>$row['file'],

                    'error'=>array
                    (
                        'id'=>$row['errors_id']
                    )
				));
				
				
				$data[] = $backtrace;
				
				if(!$return_array)
					return $data[0];
			}			
			
			return $data;
		}
		return false;
	}
}

class Backtrace extends Tobject
{
	protected $id, $type, $function, $class, $line, $file, $error;
	
	function __construct($properties=NULL)
	{		
		parent::__construct('backtraces',$properties);

        if(isset($properties['error']))
        {
            $this->error = new Error($properties['error']);
        }
	}
	
	public function getId()
	{
		return intval($this->id);
	}

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param \Error $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return \Error
     */
    public function getError()
    {
        return $this->error;
    }

	public function addRecord()
	{
		$sql =
			'INSERT INTO backtraces
			(
				id,
				errors_id,
                type,
                function,
                class,
                line,
                file
			)
			VALUES
			(
				:id,
				:errors_id,
                :type,
                :function,
                :class,
                :line,
                :file
			)';
			
		$values = array
        (
            ':id'=>$this->id,
            ':errors_id'=>$this->error->getId(),
            ':type'=>$this->type,
            ':function'=>$this->function,
            ':class'=>$this->class,
            ':line'=>$this->line,
            ':file'=>$this->file
		);
		if(pdologged_preparedQuery($sql, $values))
		{
			$this->id=Tabmin::$db->lastInsertId();
			return true;
		}
		
		return false;
	}
	
	public function updateRecord()
	{
		$sql_simpleFields = $this->generateSimpleUpdateFields(array('errors_id'));
		
		$sql =
			'UPDATE backtraces SET
				'.$sql_simpleFields->getStub().'
			WHERE id=:id';
		
		$values = $sql_simpleFields->getValues();
		$values[':id'] = $this->getId();
		//AlertSet::addBacktrace(var_export($sql, true));
		if(pdologged_preparedQuery($sql, $values) !== false)
			return true;
		return false;
	}
}
?>