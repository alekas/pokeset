<?php
class Requests extends Tobjects
{
	protected static $search_keys = array();
	protected static $table_name = 'requests';

	public static function loadAll($order_by='', $limit='')
	{
		return parent::loadByParameters('', $order_by, $limit);
	}

	/***
	 * This function loads an object based on the parameters passed
	 * It requires parameters be defined in terms of column name, condition and value
	***/
	public static function loadByParameters($parameters, $order_by='', $limit='')
	{
		return parent::loadByParameters($parameters, $order_by, $limit);
	}
		
	/***
	 * This function performs search on the specified columns and loads Requests that match teh search result
	***/	
	public static function searchLoad($search, $order_by='', $limit='')
	{		
		$where = array();
		$where[] = self::getSearchLoadWhere($search);
		return self::load(implode(' AND ', $where), true, $order_by, $limit);
	}
		
	public static function searchLoadByParameters($search, $parameters, $order_by='', $limit='')
	{
		return parent::searchLoadByParameters($search, $parameters, $order_by, $limit);
	}
		
	protected static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='')
	{
		if($where!='')
			$where='WHERE '.$where;
		
		if($order_by!='')
			$order_by='ORDER BY '.$order_by;
		
		$sql_calc_found_rows='';
		if($limit!='')
		{
			$limit='LIMIT '.$limit;
			$sql_calc_found_rows='SQL_CALC_FOUND_ROWS';
		}
		
		$sql=
			'SELECT DISTINCT '.$sql_calc_found_rows.'
				requests.id,
                domain,
                page_requested,
                timestamp,
                method,
                referrer,
                user_agent,
                remote_address,
                request_time,
                imap_uid
			FROM
				requests
			'.$where.'
			'.$order_by.'
			'.$limit;
		
		self::$found_rows=0;
		$data=array();
		if($rs = pdologged_preparedQuery($sql, $values))
		{
			if(!empty($limit))
			{
				$rs_count = pdologged_query('SELECT FOUND_ROWS()');
				if($row = $rs_count->fetch(PDO::FETCH_NUM))
					self::$found_rows=$row[0];
			}
			else
				self::$found_rows = $rs->rowCount();
			
			while($row = $rs->fetch(PDO::FETCH_ASSOC))
			{			

				$request = new Request(array
				(
                    'id'=>$row['id'],
                    'domain'=>$row['domain'],
                    'page_requested'=>$row['page_requested'],
                    'timestamp'=>$row['timestamp'],
                    'method'=>$row['method'],
                    'referrer'=>$row['referrer'],
                    'user_agent'=>$row['user_agent'],
                    'remote_address'=>$row['remote_address'],
                    'request_time'=>$row['request_time'],
                    'imap_uid'=>$row['imap_uid']
				));
				
				
				$data[] = $request;
				
				if(!$return_array)
					return $data[0];
			}			
			
			return $data;
		}
		return false;
	}
}

class Request extends Tobject
{
	protected $id, $domain, $timestamp, $method, $referrer, $user_agent;
    protected $remote_address, $request_time, $page_requested, $imap_uid;
	
	function __construct($properties=NULL)
	{		
		parent::__construct('requests',$properties);
	}
	
	public function getId()
	{
		return intval($this->id);
	}

    /**
     * @return int
     */
    public function getImapUid()
    {
        return $this->imap_uid;
    }

    /**
     * @return int
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }

    /**
     * @return float
     */
    public function getRequestTime()
    {
        return $this->request_time;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getReferrer()
    {
        return $this->referrer;
    }

    /**
     * @return string
     */
    public function getRemoteAddress()
    {
        return $this->remote_address;
    }

    /**
     * @return string
     */
    public function getPageRequested()
    {
        return $this->page_requested;
    }

	
	public function addRecord()
	{
		$sql =
			'INSERT INTO requests
			(
                domain,
                page_requested,
                timestamp,
                method,
                referrer,
                user_agent,
                remote_address,
                request_time,
                imap_uid
			)
			VALUES
			(
                :domain,
                :page_requested,
                :timestamp,
                :method,
                :referrer,
                :user_agent,
                :remote_address,
                :request_time,
                :imap_uid
			)';
			
		$values = array
        (
            ':domain'=>$this->domain,
            ':page_requested'=>$this->page_requested,
            ':timestamp'=>$this->timestamp,
            ':method'=>$this->method,
            ':referrer'=>$this->referrer,
            ':user_agent'=>$this->user_agent,
            ':remote_address'=>$this->remote_address,
            ':request_time'=>$this->request_time,
            ':imap_uid'=>$this->imap_uid
		);
		if(pdologged_preparedQuery($sql, $values))
		{
			$this->id=Tabmin::$db->lastInsertId();
			return true;
		}
		
		return false;
	}
	
	public function updateRecord()
	{
		$sql_simpleFields = $this->generateSimpleUpdateFields();
		
		$sql =
			'UPDATE requests SET
				'.$sql_simpleFields->getStub().'
			WHERE id=:id';
		
		$values = $sql_simpleFields->getValues();
		$values[':id'] = $this->getId();
		//AlertSet::addRequest(var_export($sql, true));
		if(pdologged_preparedQuery($sql, $values) !== false)
			return true;
		return false;
	}
}
?>