<?php
class Moves extends Tobjects
{
    protected static $search_keys = array('');
    protected static $table_name = 'move';

    /***
     * This function loads an object based on the parameters passed
     * It requires parameters be defined in terms of column name, condition and value
     ***/

    protected static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='')
    {
        if($where!='')
            $where='WHERE '.$where;

        if($order_by!='')
            $order_by='ORDER BY '.$order_by;

        $sql_calc_found_rows='';
        if($limit!='')
        {
            $limit='LIMIT '.$limit;
            $sql_calc_found_rows='SQL_CALC_FOUND_ROWS';
        }

        $sql=
            'SELECT DISTINCT '.$sql_calc_found_rows.'
				id,
                types_id,
                name,
                accuracy,
                base_power,
                category,
                pp,
                priority,
                target,
                damage,
                description,
                short_description
			FROM
				moves
			'.$where.'
			'.$order_by.'
			'.$limit;

        self::$found_rows=0;
        $data=array();
        if($rs = pdologged_preparedQuery($sql, $values))
        {
            if(!empty($limit))
            {
                $rs_count = pdologged_query('SELECT FOUND_ROWS()');
                if($row = $rs_count->fetch(PDO::FETCH_NUM))
                    self::$found_rows=$row[0];
            }
            else
                self::$found_rows = $rs->rowCount();

            while($row = $rs->fetch(PDO::FETCH_ASSOC))
            {

                $item = new Move(array
                (
                    'id'=>$row['id'],
                    'name'=>$row['name'],
                    'accuracy'=>$row['accuracy'],
                    'base_power'=>$row['base_power'],
                    'category'=>$row['category'],
                    'pp'=>$row['pp'],
                    'priority'=>$row['priority'],
                    'target'=>$row['target'],
                    'damage'=>$row['damage'],
                    'description'=>$row['description'],
                    'short_description'=>$row['short_description'],
                    'type'=>array
                    (
                        'id'=>$row['type']
                    )
                ));

                $data[] = $item;

                if(!$return_array)
                    return $data[0];
            }

            return $data;
        }
        return false;
    }
}

class Move extends Tobject
{
    protected $id, $type, $name, $accuracy, $base_power, $category, $pp, $priority;
    protected $target, $damage, $description, $short_description;

    function __construct($properties=NULL)
    {
        parent::__construct('move',$properties);
        if(isset($properties['type']))
        {
            $this->type = new Type($properties['type']);
        }
    }

    /**
     * @return mixed
     */
    public function getAccuracy()
    {
        return $this->accuracy;
    }

    /**
     * @return \Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getBasePower()
    {
        return $this->base_power;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function getDamage()
    {
        return $this->damage;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPp()
    {
        return $this->pp;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->short_description;
    }

    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }



    public function add()
    {
        $sql =
            'INSERT INTO moves
            (
                types_id,
                name,
                accuracy,
                base_power,
                category,
                pp,
                priority,
                target,
                damage,
                description,
                short_description
            )
            VALUES
            (
                :types_id,
                :name,
                :accuracy,
                :base_power,
                :category,
                :pp,
                :priority,
                :target,
                :damage,
                :description,
                :short_description
            )';

        $values = array(
            ':types_id'=>$this->type->getId(),
            ':name'=>$this->name,
            ':accuracy'=>$this->accuracy,
            ':base_power'=>$this->base_power,
            ':category'=>$this->category,
            ':pp'=>$this->pp,
            ':priority'=>$this->priority,
            ':target'=>$this->target,
            ':damage'=>$this->damage,
            ':description'=>$this->description,
            ':short_description'=>$this->short_description,
        );
        if(pdologged_preparedQuery($sql, $values))
        {
            $this->id=Tabmin::$db->lastInsertId();
            return true;
        }

        return false;
    }

    public function update()
    {
        return true;
    }
}
?>