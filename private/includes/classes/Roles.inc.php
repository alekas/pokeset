<?php
class Roles extends Tobjects
{
    protected static $search_keys = array('roles.role');
    protected static $table_name = 'roles';
	
	public static function loadAll($order_by='', $limit='', $load_hidden = false)
	{
		if (!$load_hidden)
			return self::load('roles.hidden=FALSE', NULL, true, $order_by, $limit);
		else
			return self::load('', NULL, true, $order_by, $limit);
	}
	
	
	protected static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='')
	{
		if($where!='')
			$where='WHERE '.$where;
		
		if($order_by!='')
			$order_by='ORDER BY '.$order_by;
		
		$sql =
			'SELECT
				id,
				hidden,
				developer,
				role
			FROM
				roles
			'.$where.'
			'.$order_by;
		
		$data=array();
		if($rs=pdologged_preparedQuery($sql, $values))
		{
			while($row = $rs->fetch(PDO::FETCH_ASSOC))
			{
				$data[] = new Role(array
				(
					'id'=>$row['id'],
					'hidden'=>$row['hidden'],
					'developer'=>$row['developer'],
					'role'=>$row['role']
				));
				if(!$return_array)
					return $data[0];
			}
			return $data;
		}
		return false;
	}
}

class Role extends Tobject
{
	protected $id, $hidden, $developer, $role;
	
	public function __construct($properties)
	{
		foreach($properties as $property=>$value)
		{
			if(property_exists($this, $property))
				$this->{"$property"}=$value;
		}
	}
	
	public function getId()
	{
		return intval($this->id);
	}
	
	public function getRole()
	{
		return htmlentitiesUTF8($this->role);
	}
	
	public function isHidden()
	{
		return (bool)($this->hidden);
	}
	
	public function isDeveloper()
	{
		return (bool)($this->developer);
	}
	
	public function getDeveloper()
	{
		return (bool)($this->developer);
	}
	
	
	
	
	public function addRecord()
	{
		return false;
	}
	
	public function updateRecord()
	{
		return false;
	}
	public function delete()
	{
		return false;
	}
}
?>