<?php
class Errors extends Tobjects
{
	protected static $search_keys = array('errors.first_name', 'errors.last_name');
	protected static $table_name = 'errors';

	public static function loadAll($order_by='', $limit='')
	{
		return parent::loadByParameters('', $order_by, $limit);
	}

	/***
	 * This function loads an object based on the parameters passed
	 * It requires parameters be defined in terms of column name, condition and value
	***/
	public static function loadByParameters($parameters, $order_by='', $limit='')
	{
		return parent::loadByParameters($parameters, $order_by, $limit);
	}
		
	/***
	 * This function performs search on the specified columns and loads Errors that match teh search result
	***/	
	public static function searchLoad($search, $order_by='', $limit='')
	{		
		$where = array();
		$where[] = self::getSearchLoadWhere($search);
		return self::load(implode(' AND ', $where), true, $order_by, $limit);
	}
		
	public static function searchLoadByParameters($search, $parameters, $order_by='', $limit='')
	{
		return parent::searchLoadByParameters($search, $parameters, $order_by, $limit);
	}
		
	protected static function load($where='', $values=NULL, $return_array=false, $order_by='', $limit='')
	{
		if($where!='')
			$where='WHERE '.$where;
		
		if($order_by!='')
			$order_by='ORDER BY '.$order_by;
		
		$sql_calc_found_rows='';
		if($limit!='')
		{
			$limit='LIMIT '.$limit;
			$sql_calc_found_rows='SQL_CALC_FOUND_ROWS';
		}
		
		$sql=
			'SELECT DISTINCT '.$sql_calc_found_rows.'
				errors.id,
				error.requests_id,
                type,
                message,
                line,
                file
			FROM
				errors
			'.$where.'
			'.$order_by.'
			'.$limit;
		
		self::$found_rows=0;
		$data=array();
		if($rs = pdologged_preparedQuery($sql, $values))
		{
			if(!empty($limit))
			{
				$rs_count = pdologged_query('SELECT FOUND_ROWS()');
				if($row = $rs_count->fetch(PDO::FETCH_NUM))
					self::$found_rows=$row[0];
			}
			else
				self::$found_rows = $rs->rowCount();
			
			while($row = $rs->fetch(PDO::FETCH_ASSOC))
			{			

				$error = new Error(array
				(
                    'id'=>$row['id'],
                    'type'=>$row['type'],
                    'message'=>$row['message'],
                    'line'=>$row['line'],
                    'file'=>$row['file'],

                    'request'=>array
                    (
                        'id'=>$row['requests_id']
                    )
				));
				
				
				$data[] = $error;
				
				if(!$return_array)
					return $data[0];
			}			
			
			return $data;
		}
		return false;
	}
}

class Error extends Tobject
{
	protected $id, $type, $message, $line, $file, $request;
	
	function __construct($properties=NULL)
	{		
		parent::__construct('errors',$properties);

        if(isset($properties['request']))
        {
            $this->request = new Request($properties['request']);
        }
	}
	
	public function getId()
	{
		return intval($this->id);
	}

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param \Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return \Request
     */
    public function getRequest()
    {
        return $this->request;
    }

	public function addRecord()
	{
		$sql =
			'INSERT INTO errors
			(
				id,
				requests_id,
                type,
                message,
                line,
                file
			)
			VALUES
			(
				:id,
				:requests_id,
                :type,
                :message,
                :line,
                :file
			)';
			
		$values = array
        (
            ':id'=>$this->id,
            ':requests_id'=>$this->request->getId(),
            ':type'=>$this->type,
            ':message'=>$this->message,
            ':line'=>$this->line,
            ':file'=>$this->file
		);
		if(pdologged_preparedQuery($sql, $values))
		{
			$this->id=Tabmin::$db->lastInsertId();
			return true;
		}
		
		return false;
	}
	
	public function updateRecord()
	{
		$sql_simpleFields = $this->generateSimpleUpdateFields(array('requests_id'));
		
		$sql =
			'UPDATE errors SET
				'.$sql_simpleFields->getStub().'
			WHERE id=:id';
		
		$values = $sql_simpleFields->getValues();
		$values[':id'] = $this->getId();
		//AlertSet::addError(var_export($sql, true));
		if(pdologged_preparedQuery($sql, $values) !== false)
			return true;
		return false;
	}
}
?>