<?php
#define('MOBILE_SITE_URL', 'm.site_name.com');
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_STRICT);
if(!empty($_SERVER['ELYK_ENV']))
    define('ELYK_ENV', $_SERVER['ELYK_ENV']);
else
    define('ELYK_ENV', 'dev');

if(!isset($config_exclusions))
	$config_exclusions=array('wordpress');

if(!in_array('session', $config_exclusions))
	session_start();
ini_set('default_charset', 'UTF-8');
date_default_timezone_set('America/New_York');

$config_suffixes = array('-env', '');
foreach($config_suffixes as $suffix)
{
    if(empty($config_xml_file))
    {
        $config_xml=$_SERVER['DOCUMENT_ROOT'].'/../private/config'.$suffix.(!empty($suffix) && ELYK_ENV!=''?'-'.ELYK_ENV :'').'.xml';
    }
    else
        $config_xml=$_SERVER['DOCUMENT_ROOT'].'/../private/'.$config_xml_file;

    if(file_exists($config_xml) && is_file($config_xml))
    {
        $config_xml = simplexml_load_string(file_get_contents($config_xml));
    }
    else
        continue;
    foreach($config_xml->constant as $constant)
    {
        if (!defined((string)$constant['name']))
            define((string)$constant['name'], (string)$constant);
    }

    if (defined('MODULE_ROOT') && defined('DOCUMENT_ROOT') && !defined('MODULE_ROOT_TRANSLATED'))
        define('MODULE_ROOT_TRANSLATED', '/'.str_replace(DOCUMENT_ROOT, '', MODULE_ROOT));

    foreach($config_xml->class as $class)
        require_once(CLASS_ROOT.$class['file']);
}

define('TABMIN', (preg_match('/^\/tabmin\//', $_SERVER['PHP_SELF'])!=0));


if(!in_array('wordpress', $config_exclusions))
{
	if(!defined('WP_USE_THEMES') && defined('WP_ROOT') && !TABMIN)
	{
		define('WP_USE_THEMES', false);
		require WP_ROOT.'wp-load.php';
	}
}

require_once 'D:/eLYK projects/_lib/ErrorSet/ErrorSet.inc.php';
require_once GLOBAL_ROOT.'classes/Tabmin.inc.php';
require_once GLOBAL_ROOT.'classes/AuthorizeNet.inc.php';
require_once GLOBAL_ROOT.'classes/AlertSet.inc.php';
require_once GLOBAL_ROOT.'classes/Validator.inc.php';
require_once GLOBAL_ROOT.'classes/Format.inc.php';
require_once GLOBAL_ROOT.'classes/Display.inc.php';
require_once GLOBAL_ROOT.'classes/XSRF.inc.php';
require_once GLOBAL_ROOT.'classes/TemplateSet.inc.php';
require_once GLOBAL_ROOT.'util.inc.php';
require_once GLOBAL_ROOT.'pdo.inc.php';
require_once GLOBAL_ROOT.'recaptcha.inc.php';
require_once GLOBAL_ROOT.'class_sendmail.inc.php';
require_once GLOBAL_ROOT.'classes/XMLMailThing.inc.php';
require_once GLOBAL_ROOT.'mobile_device_detect.php';



try
{
	Tabmin::$db = new PDO('mysql:host='.TABMIN_DB_HOST.';dbname='.TABMIN_DB_NAME.';charset=utf8', TABMIN_DB_USERNAME, TABMIN_DB_PASSWORD);
	Tabmin::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	Tabmin::$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}
catch (Exception $e)
{
    print_r(Tabmin::$db);
	//error handler here
    die('can not create db connection');
}

if(DEBUG)
	ErrorSet::$display=true;
else
	ErrorSet::$email=ERROR_REPORTING_EMAIL;

if(get_magic_quotes_gpc())
{
	$_GET=stripslashes_deep($_GET);
	$_POST=stripslashes_deep($_POST);
	$_COOKIE=stripslashes_deep($_COOKIE);
}
?>
