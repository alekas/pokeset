<?
include $_SERVER['DOCUMENT_ROOT'].'/../private/includes/config.inc.php';

$poke = PokemonCollection::loadAll('number');

?>
<table>
    <tr>
        <th colspan="2"></th>
        <th colspan="5">Base Stats</th>
    </tr>
    <tr>
        <th></th>
        <th>Num</th>
        <th>Pokemon</th>
        <th>HP</th>
        <th><abbr title="Attack">Atk</abbr></th>
        <th><abbr title="Defense">Def</abbr></th>
        <th><abbr title="Special Attack">SpA</abbr></th>
        <th><abbr title="Special Defense">SpD</abbr></th>
        <th><abbr title="Speed">Spe</abbr></th>
    </tr>
<?
foreach($poke as $i=>$pokemon)
{
?>
    <tr style="background-color: <?php echo $i%2==0 ? '#eee' : '#ccc'?>">
        <td><img src="/images/pokemon/<?php P::out($pokemon->getSprite()) ?>"></td>
        <td><?php P::out($pokemon->getNumber()) ?></td>
        <td><?php P::out($pokemon->getPrettyName()) ?></td>
        <td><?php P::out($pokemon->getHp()) ?></td>
        <td><?php P::out($pokemon->getAttack()) ?></td>
        <td><?php P::out($pokemon->getDefense()) ?></td>
        <td><?php P::out($pokemon->getSpecialAttack()) ?></td>
        <td><?php P::out($pokemon->getSpecialDefense()) ?></td>
        <td><?php P::out($pokemon->getSpeed()) ?></td>
    </tr>
<?
}
?>
</table>