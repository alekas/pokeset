<?php
ini_set('display_errors', true);
error_reporting(E_ALL);
include $_SERVER['DOCUMENT_ROOT'].'/../private/includes/config.inc.php';
ErrorSet::$display = true;
/**
 * Created By: Andreas
 * Date: 6/12/14
 * Time: 5:46 PM
 */

//$json = json_decode(file_get_contents('movedex.js'), true);
$json = json_decode(file_get_contents('pokedex.js'), true);
$columns = array();

$types = Types::loadAll();
$types_list = array();
foreach($types as $type)
{
    $types_list[$type->getName()] = $type->getId();
}

foreach($json as $i=>$item)
{
    if($item['num'] < 0)
    {
        continue;
    }
    $mon_types = array();
    foreach($item['types'] as $type)
    {
        $mon_types[] = new Type(array
        (
            'id'=>$types_list[$type]
        ));
    }

    $pokemon = new Pokemon(array
    (
        'number'=>$item['num'],
        'hp'=>$item['baseStats']['hp'],
        'attack'=>$item['baseStats']['atk'],
        'defense'=>$item['baseStats']['def'],
        'special_attack'=>$item['baseStats']['spa'],
        'special_defense'=>$item['baseStats']['spd'],
        'speed'=>$item['baseStats']['spe'],
        'pretty_name'=>$item['name'],
        'name'=>$i,
        'types'=>$mon_types
    ));

    if($pokemon->add())
    {
        print('Added '.$pokemon->getPrettyName().'<br>');
    }
    else
    {
        print('<b>Failed '.$pokemon->getPrettyName().'</b><br>');
    }
    /*
    $move = new Move(array
    (
        'id'=>$item['id'],
        'name'=>$item['name'],
        'accuracy'=>$item['accuracy'],
        'base_power'=>$item['basePower'],
        'category'=>$item['category'],
        'pp'=>$item['pp'],
        'priority'=>$item['priority'],
        'target'=>$item['target'],
        'damage'=>$item['damage'],
        'description'=>$item['desc'],
        'short_description'=>$item['shortDesc'],
        'type'=>array
        (
            'id'=>$types_list[$item['type']]
        )
    ));

    if($move->add())
    {
        print('Added '.$move->getName().'<br>');
    }
    else
    {
        print('<b>Failed '.$move->getName().'</b><br>');
    }*/
}

echo join('<br>', $columns);
Console::add(json_last_error_msg());
Console::add($json);
Console::add(file_get_contents('pokedex.js'));
?>
hello